from flask import Flask
from flask import request
from flask import render_template
import youtube_dl

app = Flask(__name__)

@app.route('/')
def my_form():
    h1DATA = "Grab A Video"
    return render_template("my-form.html", H1DATA = h1DATA)

@app.route('/', methods=['POST'])
def my_form_post():

    theURL = request.form['text']
    theQUAL = request.form['qualSelect']
    if theQUAL == "1080":
        ydl_opts = {
            'format': 'bestvideo[height <=? 1080]+bestaudio[ext =? m4a]/best',
            'postprocessors': [{
                'key': 'ExecAfterDownload',
                'exec_cmd': 'mv /app/{} /app/videodir/'
            }]
        }
    elif theQUAL == "720":
        ydl_opts = {
            'format': 'bestvideo[height <=? 720]+bestaudio[ext =? m4a]/best',
            'postprocessors': [{
                'key': 'ExecAfterDownload',
                'exec_cmd': 'mv /app/{} /app/videodir/'
            }]
        }
    elif theQUAL == "480":
        ydl_opts = {
            'format': 'bestvideo[height <=? 480]+bestaudio[ext =? m4a]/best',
            'postprocessors': [{
                'key': 'ExecAfterDownload',
                'exec_cmd': 'mv /app/{} /app/videodir/'
            }]
        }
    else:
        ydl_opts = {
            'format': 'bestvideo[height <=? 720]+bestaudio[ext =? m4a]/best',
            'postprocessors': [{
                'key': 'ExecAfterDownload',
                'exec_cmd': 'mv /app/{} /app/videodir/'
            }]
        }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(['{}'.format(theURL)])
        
    h1DATA = "Grab A Video"
    return render_template("my-form.html", H1DATA = h1DATA)






if __name__ == '__main__':
    app.run(threaded=True, host='0.0.0.0', port=80)
